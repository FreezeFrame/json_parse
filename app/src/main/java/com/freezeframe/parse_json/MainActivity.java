package com.freezeframe.parse_json;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
	private SwipeRefreshLayout mSwipeRefreshLayout;
	JSONObject jsonobject;
	JSONArray jsonarray;
	ListView listview;
	ListViewAdapter adapter;
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> arraylist;
	static String id = "id";
	static String name = "name";
	static String version = "version";
	static String image = "image";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		mSwipeRefreshLayout.setOnRefreshListener(this);


		// runtime permissions для Android 6.0
			if (ContextCompat.checkSelfPermission(this,
					Manifest.permission.READ_EXTERNAL_STORAGE)
					!= PackageManager.PERMISSION_GRANTED)

			{

				ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
				Toast toast = Toast.makeText(getApplicationContext(),
						"Нет разрешения на чтение кеша, установить и обновите страницу жестом вниз", Toast.LENGTH_LONG );
				toast.show();
			}

	// запускаем поток

		new DownloadJSON().execute();
	}
// Refresh
	@Override
	public void onRefresh() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				new DownloadJSON().execute();
				mSwipeRefreshLayout.setRefreshing(false);
			}
		}, 100);
	}

	// работаем в потоке
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// создаем прогресс диалог на время выполнения задачи
			mProgressDialog = new ProgressDialog(MainActivity.this);
			mProgressDialog.setTitle("Загрузка Json");
			mProgressDialog.setMessage("Подождите...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// создание array
			arraylist = new ArrayList<HashMap<String, String>>();
			// получение json объектов из URL адреса
			jsonobject = JSONfunctions
					.getJSONfromURL("https://dl.dropboxusercontent.com/s/2kwni1y9iclkmjj/server_data.txt");

			try {
				//разбираем массив body
				jsonarray = jsonobject.getJSONArray("body");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					jsonobject = jsonarray.getJSONObject(i);
					//
					map.put("id", jsonobject.getString("id"));
					map.put("name", jsonobject.getString("name"));
					map.put("version", jsonobject.getString("version"));
					map.put("image", jsonobject.getString("image"));
					// засовываем данные в массив
					arraylist.add(map);
				}
			} catch (JSONException e) {
				// при неудаче показываем лог Error
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			listview = (ListView) findViewById(R.id.listview);
			// показываем результат в ListViewAdapter.java
			adapter = new ListViewAdapter(MainActivity.this, arraylist);
			listview.setAdapter(adapter);
			// уничтожаем прогресс диалог
			mProgressDialog.dismiss();
		}
	}
}