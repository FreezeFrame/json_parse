package com.freezeframe.parse_json;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewAdapter extends BaseAdapter {


	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	ImageLoader imageLoader;
	HashMap<String, String> resultp = new HashMap<String, String>();

	public ListViewAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		imageLoader = new ImageLoader(context);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		//
		TextView id;
		TextView name;
		TextView version;
		ImageView image;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.listview_item, parent, false);
		// считываем позицию
		resultp = data.get(position);


		id = (TextView) itemView.findViewById(R.id.id);
		name = (TextView) itemView.findViewById(R.id.name);
		version = (TextView) itemView.findViewById(R.id.version);


		image = (ImageView) itemView.findViewById(R.id.flag);


		id.setText(resultp.get(MainActivity.id));
		name.setText(resultp.get(MainActivity.name));
		version.setText(resultp.get(MainActivity.version));

		imageLoader.DisplayImage(resultp.get(MainActivity.image), image);
		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// получаем позицию
				resultp = data.get(position);
				Intent intent = new Intent(context, SingleItemView.class);
				// передача данных id
				intent.putExtra("id", resultp.get(MainActivity.id));
				// передача данных name
				intent.putExtra("name", resultp.get(MainActivity.name));
				// передача данных version
				intent.putExtra("version",resultp.get(MainActivity.version));
				// передача данных image
				intent.putExtra("image", resultp.get(MainActivity.image));
				// запуск класса SingleItemView
				context.startActivity(intent);

			}
		});
		return itemView;
	}
}
