package com.freezeframe.parse_json;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleItemView extends AppCompatActivity {

	String id;
	String name;
	String version;
	String image;
	ImageLoader imageLoader = new ImageLoader(this);
	CollapsingToolbarLayout collapsingToolbarLayout;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.singleitemview);
		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

		// объявляем коллапс тулбар
		collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
		// статус бар делаем прозрачным чтобы картинка наезжала на него
		collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		// установим иконку кнопки назад и обработаем клик по ней как finish
		toolbar.setNavigationIcon(R.drawable.nazad);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Intent i = getIntent();

		id = i.getStringExtra("id");

		name = i.getStringExtra("name");

		version = i.getStringExtra("version");

		image = i.getStringExtra("image");


		TextView txtid = (TextView) findViewById(R.id.id);
		TextView txtname = (TextView) findViewById(R.id.name);
		TextView txtversion = (TextView) findViewById(R.id.version);


		ImageView imgflag = (ImageView) findViewById(R.id.flag);

		// отобразим переданные данные в textView а Image установим в ImageView к коллапс тулбаре
		txtid.setText("ID: "+id);
		txtname.setText("Name: "+name);
		txtversion.setText("Version: "+version);

		imageLoader.DisplayImage(image, imgflag);
	}
	// раз уж материальный дизайн то создал FAB и дал ему возможность Shared, просто тект.
	public void share (View view){
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, "Это приложение было создано как тестовое задание по загрузке и разбора Json\n\n Material Design, ImageCache, ListViewAdapter");
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}
}